
require 'minitest/autorun'
require 'test_xml/mini_test'

# DEBUG=1 ruby -Ilib -rdebug spec/mobileconfig_spec.rb
# up 2 ; list
# require 'minitest/debugger' if ENV['DEBUG']

require 'mobileconfig'

describe MobileConfig, 'new' do
  before do
    @default = MobileConfig.new
  end

  it 'has correct defaults' do
    @default[:version].must_equal 1
    @default[:type].must_equal 'Configuration'
    @default[:security].must_equal false
    @default[:content].must_equal []
  end

  it 'can override version' do
    MobileConfig.new(version: 2)[:version].must_equal 2
  end
end

describe MobileConfig, 'email' do
  before do
    @default = MobileConfig.new(identifier: 'foo')
    @default = @default.email
  end

  it 'has correct defaults' do
    @default[:content].first[:identifier].must_equal 'foo.email'

    @default[:content].first[:email_type].must_equal :imap
    @default[:content].first[:incoming_auth].must_equal :password

    @default[:content].first[:incoming_port].must_equal 993
    @default[:content].first[:incoming_ssl].must_equal true

    @default[:content].first[:outgoing_auth].must_equal :password
    @default[:content].first[:outgoing_port].must_equal 587
    @default[:content].first[:outgoing_ssl].must_equal true
    @default[:content].first[:same_passwords].must_equal true

    @default[:content].first[:prevent_app].must_equal false
    @default[:content].first[:prevent_move].must_equal false
    @default[:content].first[:smime].must_equal true
  end
end

describe MobileConfig, 'to_xml' do
  before do
    @xml = MobileConfig.new.to_xml
  end

  it 'has processing' do
    @xml.must_match /^<\?xml\s+version="1.0"\s+encoding="UTF-8"\s*\?>/
  end

  it 'has correct doctype' do
    @xml.must_match %r{<!DOCTYPE\s+plist\s+PUBLIC\s+"-//Apple//DTD PLIST 1.0//EN"\s+"http://www.apple.com/DTDs/PropertyList-1.0.dtd"\s*>}
  end

  it 'has plist' do
    #assert_xml_structure_contain @xml, '<plist version="1.0"><dict></dict></plist>'
    '<plist version="1.0"><dict></dict></plist>'.must_contain_xml_structure @xml
    #@xml.must_contain_xml_structure '<?xml version="1.0" encoding="UTF-8"?>' +
    #    '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">' +
    #    '<plist version="1.0"><dict></dict></plist>'
  end
end

