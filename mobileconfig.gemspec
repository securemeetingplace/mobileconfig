
Gem::Specification.new do |s|
  s.name = 'mobileconfig'
  s.version = '0.0.1'
  s.summary = 'Apple mobileconfig'
  s.description = 'Generate Apple mobileconfig files'
  s.authors = [ 'Mike Carlton' ]
  s.email = [ 'mike@subrosa.email' ]
  s.homepage = 'http://www.subrosa.email/mobileconfig'

  # s.add_dependency('rest-client', '~> 1.4')

  # s.add_development_dependency('shoulda', '~> 3.4.0')
  # s.add_development_dependency('test-unit')
  # s.add_development_dependency('rake')

  s.files = [ 'lib/mobileconfig.rb' ]
  #s.test_files    = `git ls-files -- test/*`.split("\n")
  #s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.license = 'MIT'
end
