
require 'builder'
require 'uuidtools'
require 'openssl'

include Builder
include UUIDTools

class MobileConfig
  ENUM = {
        imap: 'EmailTypeIMAP',
    password: 'EmailAuthPassword',
      pkcs12: 'com.apple.security.pkcs12',
       pkcs1: 'com.apple.security.pkcs1',
  }

  KEYS = {
    # general
                  name: 'PayloadDisplayName',
            identifier: 'PayloadIdentifier',
          organization: 'PayloadOrganization',
           description: 'PayloadDescription',
              security: 'PayloadRemovalDisallowed',
                  type: 'PayloadType',
               version: 'PayloadVersion',
                  uuid: 'PayloadUUID',
               content: 'PayloadContent',

    # email
     email_description: 'EmailAccountDescription',
	    email_type: 'EmailAccountType',
           imap_prefix: 'IncomingMailServerIMAPPathPrefix',
            email_name: 'EmailAccountName',
               address: 'EmailAddress',

         incoming_auth: 'IncomingMailServerAuthentication',
         incoming_host: 'IncomingMailServerHostName',
         incoming_port: 'IncomingMailServerPortNumber',
          incoming_ssl: 'IncomingMailServerUseSSL',
     incoming_username: 'IncomingMailServerUsername',

         outgoing_auth: 'OutgoingMailServerAuthentication',
         outgoing_host: 'OutgoingMailServerHostName',
         outgoing_port: 'OutgoingMailServerPortNumber',
          outgoing_ssl: 'OutgoingMailServerUseSSL',
     outgoing_username: 'OutgoingMailServerUsername',

        same_passwords: 'OutgoingPasswordSameAsIncomingPassword',

           prevent_app: 'PreventAppSheet',
          prevent_move: 'PreventMove',
                 smime: 'SMIMEEnabled',
       encryption_cert: 'SMIMEEncryptionCertificateUUID',
          signing_cert: 'SMIMESigningCertificateUUID',

    # credentials
         cert_filename: 'PayloadCertificateFileName'
  }

  def initialize(opts = nil)
    @profile = {
      type: 'Configuration',
      uuid: UUID.random_create.to_s,
      version: 1,
      security: false,
      content: [],
    }.merge(opts || {})
  end

  def [](key)
    @profile[key]
  end

  def to_xml
    xml = XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.declare! :DOCTYPE, :plist, :PUBLIC, "-//Apple//DTD PLIST 1.0//EN",
                 "http://www.apple.com/DTDs/PropertyList-1.0.dtd"
    xml.plist(version: "1.0") { |plist| add plist, @profile }
  end

  def email(opts = nil)
    email_opts = {
               name: 'IMAP Account',
        description: 'Configures email account.',
         email_type: :imap,
      incoming_auth: :password,
      incoming_port: 993,
       incoming_ssl: true,

      outgoing_auth: :password,
      outgoing_port: 587,
       outgoing_ssl: true,
     same_passwords: true,

        prevent_app: false,
       prevent_move: false,
              smime: true,

         identifier: @profile[:identifier] + '.email',
       organization: @profile[:organization],
            version: @profile[:version],
               type: 'com.apple.mail.managed',
               uuid: UUID.random_create.to_s,
    }.merge(opts || {})

    @profile[:content] << email_opts
    self
  end

  def credentials(opts = nil)
    credential_opts = {
        description: 'Provides device authentication (certificate or identity).',

         identifier: @profile[:identifier] + '.credential',
       organization: @profile[:organization],
            version: @profile[:version],
               uuid: UUID.random_create.to_s,
    }.merge(opts || {})

    @profile[:content] << credential_opts
    self
  end

  private

  def add(node, obj)
    case obj
      when Array
        node.array do |array|
          obj.each { |elt| add array, elt }
        end
      when Hash
        node.dict do |dict|
          obj.each do |key, value|
            dict.key(KEYS[key] || key.to_s)
            add dict, value
          end
        end
      when TrueClass
        node.true
      when FalseClass
        node.false
      when Symbol
        node.string(ENUM[obj] || obj.to_s)
      when String
        node.string(obj)
      when Integer
        node.integer(obj)
      when OpenSSL::X509::Certificate
        node.data(
          obj.to_pem.gsub(/-----.*-----\n/, '').chomp
        )
      else
        raise ArgumentError
    end
  end
end

